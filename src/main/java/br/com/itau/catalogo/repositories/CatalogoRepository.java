package br.com.itau.catalogo.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.catalogo.models.Catalogo;	

public interface CatalogoRepository extends CrudRepository<Catalogo, String> {
	
	public Optional<Catalogo> findByNome(String nome);
}
		