package br.com.itau.catalogo.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.catalogo.models.Catalogo;
import br.com.itau.catalogo.repositories.CatalogoRepository;


@Service
public class CatalogoService {
	@Autowired
	CatalogoRepository catalogoRepository;
	
	public Catalogo cadastrar(Catalogo catalogo) {
		return catalogoRepository.save(catalogo);
	}
	
	public Optional<Catalogo> obterCatalogo(String nome){
		return catalogoRepository.findByNome(nome);
	}
}
