package br.com.itau.catalogo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Value;

@Entity
public class Catalogo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public String nome;
	
	@NotBlank
	public double preco;
	
	@Value("false")
	public boolean disponivel;
	
	
	public String getNome(String nome) {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPreco(double preco) {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	

	public boolean getDisponivel(boolean disponivel) {
		return disponivel;
	}
	
	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}
	
	
}


